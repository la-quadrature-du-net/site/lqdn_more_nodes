$('a.more-nodes').click(function(){
    $elem = $(this).parent().parent().find('div.last-actu div.item-list ul');
    var from = $elem.find('li').size();
    var type = $(this).attr('data-type');
    var dossier = "-";
    var url = '/more-nodes/' + from + '/' + type + '/' + dossier;
    $.getJSON(url, function(json){
	$elem.append(json.content);
    });
    return false;
});
